package com.luv2code.springdemo.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomerRestExceptionHandler {

	
	//to handle Customer not found exception
	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleException(CustomerNotFoundException exc)
	{
		
		CustomerErrorResponse err=new CustomerErrorResponse();
		
		err.setStatus(HttpStatus.NOT_FOUND.value());
		err.setMessage(exc.getMessage());
		err.setTimestamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(err,HttpStatus.NOT_FOUND);
		
	}
	
	//to handle all other exception
	
	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleAllException(Exception exc)
	{
		
		CustomerErrorResponse err=new CustomerErrorResponse();
		
		err.setStatus(HttpStatus.BAD_REQUEST.value());
		err.setMessage(exc.getMessage());
		err.setTimestamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(err,HttpStatus.BAD_REQUEST);
	}	
	
}
