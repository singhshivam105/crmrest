package com.luv2code.springdemo.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.springdemo.entity.Customer;
import com.luv2code.springdemo.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerRestController {
    
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/customers")
	 public List<Customer> getCustomers(){
		 
		 return customerService.getCustomers();
	 }
	
	
	@GetMapping("/customers/{customerId}")
	 public Customer getCustomers(@PathVariable int customerId){
		
		
		
		 
		Customer customer=customerService.getCustomer(customerId);
		
		if(customer==null) {
			throw new CustomerNotFoundException("Customer id not found"+ customerId);
		}
		
		 return customer;
	 }
	
	@PostMapping("/customers")
	public Customer addCustomer(@RequestBody Customer theCustomer) {
		//also just in case pass id in json as o
		
		theCustomer.setId(0);
		customerService.saveCustomer(theCustomer);
		
		return theCustomer;
	}
	
	//to update
	
	@PutMapping("/customers")
	public Customer updateCustomer(@RequestBody Customer theCustomer) {
		
		customerService.saveCustomer(theCustomer);
		return theCustomer;
	}
	
	
	//to delete from customer
	@DeleteMapping("/customers/{customerID}")
	public String deleteCustomer(@PathVariable int customerID) {
		
		Customer customer=customerService.getCustomer(customerID);
		if(customer==null) {
			throw new CustomerNotFoundException("Customer id not found"+ customerID);
		}
		
		customerService.deleteCustomer(customerID);
		
		return "deleted customer id"+customerID;
	}
	
	
	
	
	
	
}
